package dataProcessing;
import java.util.ArrayList;
import java.util.List;


/**
 * This class is responsible of processing text and getting the topics from it.
 *
 */
public class MessageProcessor {
	
	static public String[] getSentences(String message){
		return message.split("((([\n\r\t])|([.,!?:]+ +))+)|([.,!?:\u0085]+$)");
	}
	
	static public List<String> getUnigrams(String message){
		List<String> wordList = new ArrayList<String>();
		String[] words = message.split("[\\s\"'&-/]+");//splits by whitespaces of all kinds.
		for(String word : words){
			word = word.replaceAll("([,.!?:=()\u0085]+)","");//deletes punctuation characters.
			if(!word.isEmpty()){
				wordList.add(word);
			}
		}
		return wordList;
	}
	
	
	static public List<String> getNgrams(String message, int n){
		
		List<String> ngrams = new ArrayList<String>();
		
		for (String sentence : getSentences(message)){
			List<String> words = getUnigrams(sentence);
			if(n > words.size()){ // can't create ngrams at this size.
				continue;
			}
			for(int i=0;i<words.size()-n+1;i++){
				String curr = "";
				for(int j=0;j<n-1;j++){
					curr += words.get(i+j) + " ";
				}
				curr += words.get(i+n-1);
				ngrams.add(curr);
			}
		}
		
		return ngrams;
	}
	
	static public List<String> getPossibleNameSequences(String message){
		ArrayList<String> seqList = new ArrayList<String>();
		if (message == null) {
			return seqList;
		}
		for (String sentence : getSentences(message)){
			String curr = "";
			for(String word : getUnigrams(sentence)){
				if(!word.matches("^[A-Z\u00D8].*")){//doesnt starts with lowercase letter
					if(curr == ""){ //curr is empty
						continue;
					} else {//store curr to the list 
						seqList.add(curr);
						curr = "";
					}
				} else {
					if(curr == ""){ //curr is empty
						curr += word;
					} else { // add space
						curr += " " + word;
					}
				}
			}
			
			//any leftovers
			if(curr != ""){
				seqList.add(curr);
			}
		}	
		
		return seqList;
	}
}
