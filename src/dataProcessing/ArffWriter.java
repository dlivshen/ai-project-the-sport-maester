package dataProcessing;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.NonSparseToSparse;
import dataManagement.PostData;

/**
 * This class is responsible for exporting our processed data to .arff file which WEKA uses.
 *
 */
public class ArffWriter {
	public static String sTagPrefix = "TAG_";
	public static String sWordPrefix = "BOW_";
	private static int MIN_ATTR_FREQ = 2;
	/**
	 * @param posts list of {@link PostData} objects repressing the posts to save.
	 * @param destFileName a full absolute path to the output file.
	 * @throws Exception
	 */
	public static void savePosts(List<PostData> posts, String destFileName) throws Exception {
		//Set up attributes
		double vals[];
		FastVector atts = new FastVector();
		atts.addElement(new Attribute("date","yyyy-MM-dd--HH-mm-ss"));
		atts.addElement(new Attribute("Message",(FastVector) null));
		FastVector types = new FastVector();
		types.addElement("link");
		types.addElement("status");
		types.addElement("video");
		types.addElement("photo");
		atts.addElement(new Attribute("Type",types));
		atts.addElement(new Attribute("LikesNum"));
		atts.addElement(new Attribute("SharesNum"));
		atts.addElement(new Attribute("CommentsNum"));
		FastVector levels = new FastVector();
		for(String popularity : PostData.Popularities){
			levels.addElement(popularity);
		}
		atts.addElement(new Attribute("Popularity",levels));
		atts.addElement(new Attribute("Likes Level",levels));
		FastVector bool = new FastVector();
		bool.addElement("f");
		bool.addElement("t");
		int startOfSemAtts = 8;
		int startOfBowAtts = startOfSemAtts;
		for (String semAtt : createSemanticAttributes(posts)) {
			atts.addElement(new Attribute(sTagPrefix+semAtt,bool));
			startOfBowAtts++;
		}
		for (String bowAtt : createBagOfWordsAttributes(posts)) {
			atts.addElement(new Attribute(sWordPrefix+bowAtt,bool));
		}
		
		//instances
		Instances data = new Instances("Posts",atts,0);
		for (PostData post : posts) {
			vals = new double[data.numAttributes()];
			if (post.getDateAsString() == null ) {
				vals[0] = Instance.missingValue();
			} else {
				vals[0] = data.attribute(0).parseDate(post.getDateAsString()) ;
			}
			if (post.getMessage() == null ) {
				vals[1] = Instance.missingValue();
			} else {
				vals[1] = data.attribute(1).addStringValue(post.getMessage());
			}
			if (post.getType() == null ) {
				vals[2] = Instance.missingValue();
			} else {
				vals[2] = types.indexOf(post.getType());
			}
			if (post.getLikesNum() == null ) {
				vals[3] = Instance.missingValue();
			} else {
				vals[3] = Integer.parseInt(post.getLikesNum().toString());
			}
			if (post.getSharesNum() == null ) {
				vals[4] = Instance.missingValue();
			} else {
				vals[4] = Integer.parseInt(post.getSharesNum().toString());
			}
			if (post.getCommentsNum() == null ) {
				vals[5] = Instance.missingValue();
			} else {
				vals[5] = Integer.parseInt(post.getCommentsNum().toString());
			}
			if(post.getPopularity() == null) {
				vals[6] = Instance.missingValue();
			} else {
				vals[6] = levels.indexOf(post.getPopularity());
			}
			if(post.getLikesLevel() == null) {
				vals[7] = Instance.missingValue();
			} else {
				vals[7] = post.getLikesLevel();
			}
			for (int i = startOfSemAtts; i < startOfBowAtts; i++) {
				String att = data.attribute(i).name().substring(sTagPrefix.length());
				if (post.getSemanticTags().contains(att)) {
					vals[i] = 1;
				} else {
					vals[i] = 0;
				}
			}
			for (int i = startOfBowAtts; i < data.numAttributes(); i++) {
				String att = data.attribute(i).name().substring(sWordPrefix.length());
				if (post.getBagOfWords().contains(att)) {
					vals[i] = 1;
				} else {
					vals[i] = 0;
				}
			}
			data.add(new Instance(1.0, vals));
		}
		ArffSaver saver = new ArffSaver();
		saver.setInstances(formatInstancesToSparse(data));
		saver.setFile(new File(destFileName));
		saver.writeBatch();

	}
	
	private static Set<String> createSemanticAttributes(List<PostData> posts) {
		/*improvement - minimum frequency*/
		List<String> attrbiutes_list = new ArrayList<String>();
		Set<String> attrbiutes = new HashSet<String>();
		for (PostData post : posts) {
			attrbiutes_list.addAll(post.getSemanticTags());
		}
		for(String tag : new HashSet<String>(attrbiutes_list)){
			if(Collections.frequency(attrbiutes_list, tag) >= MIN_ATTR_FREQ){
				attrbiutes.add(tag);
			}
		}
		return attrbiutes;
	}
	
	private static Set<String> createBagOfWordsAttributes(List<PostData> posts) {
		/*improvement - minimum frequency*/
		List<String> attrbiutes_list = new ArrayList<String>();
		Set<String> attrbiutes = new HashSet<String>();
		for (PostData post : posts) {
			attrbiutes_list.addAll(post.getBagOfWords());
		}
		for(String tag : new HashSet<String>(attrbiutes_list)){
			if(Collections.frequency(attrbiutes_list, tag) >= MIN_ATTR_FREQ){
				attrbiutes.add(tag);
			}
		}
		return attrbiutes;
	}
	
	/**
	 * @param posts new, different posts that we havn't trained on
	 * @param trainInstances only used for matching the features for the test set
	 * @return new test set for testing and experimenting
	 * @throws Exception
	 */
	public static Instances createTestSet(List<PostData> posts,  Instances trainInstances) throws Exception{
		
		FastVector atts = new FastVector();
		for (int i=0;i<trainInstances.numAttributes();i++){
			Attribute att = trainInstances.attribute(i);
			atts.addElement(att);
		}
		
		FastVector types = new FastVector();
		types.addElement("link");
		types.addElement("status");
		types.addElement("video");
		types.addElement("photo");
		FastVector levels = new FastVector();
		for(String popularity : PostData.Popularities){
			levels.addElement(popularity);
		}
		
		Instances data = new Instances("Posts",atts,0);
		for (PostData post : posts) {
			double[] vals = new double[data.numAttributes()];
			for(int i=0;i<trainInstances.numAttributes();i++){
				Attribute att = trainInstances.attribute(i);
				if(att.name().equals("date")){
					if (post.getDateAsString() == null ) {
						vals[i] = Instance.missingValue();
					} else {
						vals[i] = att.parseDate(post.getDateAsString()) ;
					}
				}
				if(att.name().equals("Message")){
					if (post.getMessage() == null ) {
						vals[i] = Instance.missingValue();
					} else {
						vals[i] = att.addStringValue(post.getMessage());
					}
				}
				if(att.name().equals("Type")){
					if (post.getType() == null ) {
						vals[i] = Instance.missingValue();
					} else {
						vals[i] = types.indexOf(post.getType());
					}
				}
				if(att.name().equals("LikesNum")){
					/*special case, because of the discrete option of the likesNum*/
					if (post.getLikesNum() == null ) {
						vals[i] = Instance.missingValue();
					} else {						
						vals[i] = Integer.parseInt(post.getLikesNum().toString());						
					}
				}
				if(att.name().equals("Likes Level")){
					if(post.getLikesLevel()==null){
						vals[i] = Instance.missingValue();
					} else {
						vals[i] = post.getLikesLevel();
					}
				}
				if(att.name().equals("SharesNum")){
					if (post.getSharesNum() == null ) {
						vals[i] = Instance.missingValue();
					} else {
						vals[i] = Integer.parseInt(post.getSharesNum().toString());
					}
				}
				if(att.name().equals("CommentsNum")){
					if (post.getCommentsNum() == null ) {
						vals[i] = Instance.missingValue();
					} else {
						vals[i] = Integer.parseInt(post.getCommentsNum().toString());
					}
				}
				if(att.name().equals("Popularity")){
					if(post.getPopularity() == null) {
						vals[i] = Instance.missingValue();
					} else {
						vals[i] = levels.indexOf(post.getPopularity());
					}
				}
				if(att.name().startsWith(sTagPrefix)){
					String tag = data.attribute(i).name().substring(sTagPrefix.length());
					if (post.getSemanticTags().contains(tag)) {
						vals[i] = 1;
					} else {
						vals[i] = 0;
					}
				}
				if(att.name().startsWith(sWordPrefix)){
					String word = data.attribute(i).name().substring(sWordPrefix.length());
					if (post.getBagOfWords().contains(word)) {
						vals[i] = 1;
					} else {
						vals[i] = 0;
					}
				}
			}
			data.add(new Instance(1.0,vals));
		}
		return formatInstancesToSparse(data);
	}
	
	/**
	 * @param dataSet the instances to sparse 
	 * @return the formatted instances in sparse.
	 * @throws Exception
	 */
	public static Instances formatInstancesToSparse(Instances dataSet) throws Exception{
		NonSparseToSparse nonSparseToSparseInstance = new NonSparseToSparse(); 
		nonSparseToSparseInstance.setInputFormat(dataSet); 
		Instances sparseDataset = Filter.useFilter(dataSet, nonSparseToSparseInstance);
		return sparseDataset;
	}
}
