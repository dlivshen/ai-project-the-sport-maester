package dataProcessing;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



/**
 * This class is responsible to check if a subject is an Athlete of related entity to sport using our databases.
 *
 */
public class AthleteChecker {
	Map<String, SportsItem> namesHash;
	Map<String, List<String>> nicknamesHash;
	public AthleteChecker() {
		namesHash = new HashMap<String, SportsItem>();
		nicknamesHash = new HashMap<String, List<String>>();
		readData();
	}
	
	@SuppressWarnings("unchecked")
	private  void readData() {
		for (String fileName : dbFiles) {
			JSONArray jsonArr = getDataFromFile(fileName);
			if (jsonArr == null) {
				continue;
			}
			Iterator<Object> iter = jsonArr.iterator();
			while (iter.hasNext()) {
				JSONObject json = (JSONObject) iter.next();
				if (debug) {
					System.out.println(json);
					System.out.println(fileName);
				}
				JSONArray ret = (JSONArray)json.get("name");
				Iterator<String> it = ret.iterator();
				String name = it.next();
				List<String> nicks = new ArrayList<String>();
				JSONArray items = (JSONArray)json.get("/common/topic/alias");
				Iterator<String> iter2 = items.iterator();
				while (iter2.hasNext()) {
					String str =(String)(iter2.next());
					if (nicknamesHash.containsKey(str)) {
						nicknamesHash.get(str).add(name);
					} else {
						List<String> toAdd = new ArrayList<String>();
						toAdd.add(name);
						nicknamesHash.put(str,toAdd );
					}
					nicks.add(str);
				}
				namesHash.put(name, new SportsItem(name, nicks));
			}
		}
	}
	private static String[] dbFiles = {"Freebase/athletes_part1.json", "Freebase/athletes_part2.json", "Freebase/athletes_part3.json", "Freebase/athletes_part4.json", "Freebase/athletes_part5.json", "Freebase/athletes_part6.json", "Freebase/sport_teams.json"};
	private static boolean debug = false;
	private static JSONArray getDataFromFile(String fileName) {
		JSONParser parser = new JSONParser();
		JSONObject obj;
		try {
			obj = (JSONObject)parser.parse(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		JSONArray jsonArr = (JSONArray)obj.get("result");
		return jsonArr;
	}
	
	/**
	 * @param name the nickname of the sport entity
	 * @return the full name of the entity and null if not related to sport
	 */
	public String getNameByNickName(String name) {
		List<String> res = nicknamesHash.get(name);
		if (res != null) {
			return res.get(0);
		}
		return null;
	}
	
	/**
	 * @param name the entity's name
	 * @return true if related to sport
	 */
	public boolean checkIsNameRelatedToSport(String name) {
		if (namesHash.containsKey(name) ) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param name the nick name
	 * @return all possible full-names related to the nickname
	 */
	public List<String> getNamesByNickName(String name) {
		List<String> nicks = nicknamesHash.get(name);
		if (nicks == null) {
			return new ArrayList<String>();
		} 
		return nicks;
	}
}


class SportsItem {
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getNicknames() {
		return nicknames;
	}
	public void setNicknames(List<String> nicknames) {
		this.nicknames = nicknames;
	}
	public SportsItem(String name, List<String> nicknames) {
		this.name = name;
		this.nicknames = nicknames;
	}
	String name;
	List<String> nicknames;
	
}