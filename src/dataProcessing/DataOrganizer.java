package dataProcessing;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ArffSaver;
import dataManagement.FacebookReader;
import dataManagement.PostData;
import dataManagement.PostsLoaderCSV;
import dataManagement.WikipediaReader;
import facebook4j.FacebookException;

/**
 * This class is responsible of mapping the basic fetched data into our fully operational set.
 * <br/>
 * This class is responsible of adding all the derived features: semantic tags.
 *
 */
public class DataOrganizer {
	/**
	 * This function is downloading the posts published in dates from {@code since} until {@code until}, processes them and outputs to the file the data set.
	 * @param since
	 * @param until
	 * @param destFileName
	 */
	public static void processAndSaveDataFromFacebook(Calendar since, Calendar until, String destFileName) {
		FacebookReader fr = new FacebookReader();
		List<PostData> posts = null;
		try {
			posts = fr.getPosts(since.getTime(), until.getTime()) ;
		} catch (FacebookException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		processPosts(posts);
		System.out.println("Saving file");
		try {
			ArffWriter.savePosts(posts,destFileName);
		} catch (Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("done");
	}
	
	/**
	 * This function get the basic data from file. <br/>
	 * Processes the data and outputs it to the file the data set.
	 * @param fileName
	 * @param destFileName
	 */
	public static void processAndSaveDataFromFile(String fileName, String destFileName) {
		List<PostData> posts;
		try {
			posts = PostsLoaderCSV.loadPosts(fileName); // 
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(0);
			return;
		}
		processPosts(posts);
		System.out.println("Saving file");
		try {
			ArffWriter.savePosts(posts,destFileName);
		} catch (Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("done");
	}
	
	
	public static void processAndSaveTestSetFromCSV(String fileName, String destFileName, String trainFileName) {
		List<PostData> posts;
		try {
			posts = PostsLoaderCSV.loadPosts(fileName); // 
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(0);
			return;
		}
		processPosts(posts);
		System.out.println("Saving file");
		try {
			ArffReader arffReader = new ArffReader(new FileReader(trainFileName));
			Instances trainInstances = arffReader.getData();
			Instances data = ArffWriter.createTestSet(posts, trainInstances);
			ArffSaver saver = new ArffSaver();
			saver.setInstances(data);
			saver.setFile(new File(destFileName));
			saver.writeBatch();
		} catch (Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("done");
	}
	
	
	/**
	 * Adds to post the semantic tags
	 * Firstly, tries to find the topics in the post.<br/>
	 * Then, tries to find all the topics related to sport. For every such topic gets the full name and receives the semantic tags from wikipedia.
	 * @param posts
	 */
	public static void processPosts(List<PostData> posts) {
		AthleteChecker ac = new AthleteChecker();//Read files from db to hash tables
		WikipediaReader wr = new WikipediaReader();
		System.out.println("adding tags");
		int i = 0;
		for (PostData post : posts) {
			i++;
			System.out.println("post num " + i);
			List<String> names = MessageProcessor.getPossibleNameSequences(post.getMessage());//Get the names to check the topics
			for (String name : names) {
				if (ac.checkIsNameRelatedToSport(name)) { // If we recognize the name as sports topic then we try to get wiki categories and continue
					Set<String> categories = wr.getCategories(name);
					post.getSemanticTags().addAll(categories);
					continue;
				}//If not, we assume that this name is nickname.
				List<String> possibleNames = ac.getNamesByNickName(name);
				if (possibleNames.size() == 0) { // No names from this nickname, try bigrams of this nickname
					boolean res = orgranizeDataOnSet(post,MessageProcessor.getNgrams(name, 2),name,ac,wr);
					if (!res) {
						res = orgranizeDataOnSet(post,MessageProcessor.getUnigrams(name),name,ac,wr); // if no bigrams so unigrams
					} 
					continue; // Continue to the next name
				}
				for (String n : possibleNames) {
					Set<String> l;
					l = wr.getCategories(n);
					if (l.size() != 0 ){ 
						post.getSemanticTags().addAll(l);
						break;
					}
				}
			}
		}
		System.out.println("done");
	}
	
	private static boolean orgranizeDataOnSet(PostData post,List<String> list, String name, AthleteChecker ac, WikipediaReader wr ) {
		boolean recognized = false;
		for (String unigram : list) {
			if (ac.checkIsNameRelatedToSport(unigram)) {
				Set<String> categories = wr.getCategories(unigram);
				post.getSemanticTags().addAll(categories);
				recognized = true;
				break;
			} else {
				boolean found = false;
				for (String bnick : ac.getNamesByNickName(unigram)) {
					Set<String> l;
					l = wr.getCategories(bnick);
					if (l.size() != 0 ){ 
						post.getSemanticTags().addAll(l);
						recognized = true;
						found = true;
						break;
					}
				}
				if (found) { // If we found, no need to check other options
					break;
				}
			}
		}
		return recognized;
	}

}
