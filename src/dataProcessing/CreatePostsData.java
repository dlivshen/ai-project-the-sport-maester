
package dataProcessing;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import dataManagement.FacebookReader;
import dataManagement.PostData;
import dataManagement.PostsLoaderCSV;
import dataManagement.PostsStorerCSV;



/**
 *  This is an example of how to get posts from facebook and creates a new database of posts that stored into .CSV file.
 *	<br/>
 *	This code describes how we got our training set for machine learning.
 */
public class CreatePostsData {

	
	private final static  String postsCsvFilename = "posts.csv";
	private final static String postsArffFilename = "posts.arff";
	private static Scanner scanner;
	public static void main(String[] args) throws Exception {
		List<PostData> posts;
		System.out.println("Do you want to fetch posts from facebook? (Y/N)");
		scanner = new Scanner(System.in);
		if(scanner.next().equalsIgnoreCase("y")){
			
			FacebookReader fr = new FacebookReader();
			PostsStorerCSV storer = new PostsStorerCSV(postsCsvFilename);
			
			Calendar calUntil = Calendar.getInstance();
		    calUntil.set(2015,Calendar.APRIL,1,0,0);
		    Calendar calSince = Calendar.getInstance();
		    calSince.set(2014,Calendar.JUNE,27,0,0);//stable start date!
		    
		    System.out.println("Recieving posts from facebook");
		    posts =  fr.getPosts(calSince.getTime(), calUntil.getTime());
		    
		    storer.store(posts);
		    storer.close();
		    System.out.println("done");
		    
		} else {
			posts = PostsLoaderCSV.loadPosts(postsCsvFilename);
		}
	    
	    System.out.println("Do you want to process posts to arff file? (Y/N)");
		if(scanner.next().equalsIgnoreCase("y")){
			System.out.println("processing posts to arff file");		    
		    DataOrganizer.processPosts(posts);
		    ArffWriter.savePosts(posts,"data/"+postsArffFilename);
		}     
	    
	    System.out.println("done");
	}

}

