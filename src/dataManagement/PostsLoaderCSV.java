package dataManagement;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.csvreader.CsvReader;


/**
 * This class is responsible of loading posts that stored in CSV files offline.
 *
 */
public class PostsLoaderCSV {
	protected static  String dir = "data\\";
	static public List<PostData> loadPosts(String filename) throws IOException  {
		CsvReader csvInput = new CsvReader(dir + filename,',',Charset.forName("UTF-8"));
		csvInput.readHeaders();
		
		List<PostData> posts = new ArrayList<PostData>();
		while(csvInput.readRecord()){
			PostData post = new PostData(csvInput.getValues());
			posts.add(post);
		}
		
		csvInput.close();
		return posts;		
	}

}
