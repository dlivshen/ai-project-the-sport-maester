package dataManagement;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.security.auth.login.FailedLoginException;

import org.wikipedia.Wiki;


/**
 * This class is responsible of contacting Wikipedia and getting the semantic tags according to the post's subject.
 *
 */
public class WikipediaReader {
	private static String sUserName = "ai-technion-prject";
	private static String sPassword = "123456";
	private Wiki mWiki;
	public WikipediaReader() {
		mWiki = new Wiki("en.wikipedia.org"); // create a new wiki connection to en.wikipedia.org
		mWiki.setThrottle(5000); // set the edit throttle to 0.2 Hz
		try {
			mWiki.login(sUserName, sPassword);
		} catch (FailedLoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //
	}
	
	/**
	 * @param title - the subject of the entity to get tags from
	 * @return
	 */
	public Set<String> getCategories(String title) {
		Set<String> cats = new HashSet<String>();
		String trimText = "Category:";
		try {
			for (String cat : mWiki.getCategories(title)) {
				if (!isNeededCategory(cat.substring(trimText.length()))) {
					continue;
				}
				cats.add(cat.substring(trimText.length()));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cats.add(title);
		return cats;
	}
	
	/**
	 * Filters unnecessary categories
	 * @return
	 */
	private boolean isNeededCategory(String category) {
		return ! (category.matches("Use mdy dates from.*") || category.matches("Vague or ambiguous.*") 
				|| category.matches("Commons category.*") || category.matches("CS1.*") || category.matches(".*[pP]ages.*") 
				|| category.matches(".*[wW]ikipedia.*") || category.matches(".*[wW]ikidata.*") 
				|| category.matches("Use dmy dates from.*") 	|| category.matches(".*[cC]ompanies.*") 
				|| category.matches("Subscription required.*") || category.matches("[0-9]+ births")
				|| category.matches("Use British English.*") || category.matches("[0-9]+ establishments.*")		
				||  category.matches("[0-9]+ controversies.*")	||  category.matches("EngvarB.*")	
				|| category.matches(".*[aA]rticle[ s].*") || category.matches("Association football clubs established in.*") ) ;
	}
}
