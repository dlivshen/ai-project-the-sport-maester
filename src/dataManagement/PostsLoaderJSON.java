package dataManagement;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;




/**
 * This class is responsible of loading posts that stored offline in .json files.
 *
 */
public class PostsLoaderJSON {
	protected static  String dir = "data\\";
	static public List<PostData> loadPosts(String filename) throws IOException  {
		
		JsonReader reader = new JsonReader(new FileReader(dir + filename));
		List<PostData> posts = new ArrayList<PostData>();
		
		reader.beginArray();
		while(reader.hasNext()){
			reader.beginObject();
			
			/*basic string*/
			String[] basic = new String[PostData.basicColumnNames.length];
			for(int i=0;i<basic.length;i++){
				if(!reader.nextName().equals(PostData.basicColumnNames[i])){
					reader.close();
					throw new IOException();
				}
				if(reader.peek()==JsonToken.NULL){
					reader.nextNull();
				} else {
					basic[i] = reader.nextString();
				}
			}
			PostData post = new PostData(basic);
			
			/*read bagofwords*/
			if(!reader.nextName().equals("bagOfWords")){
				reader.close();
				throw new IOException();
			}
			reader.beginArray();
			while(reader.hasNext()){
				post.getBagOfWords().add(reader.nextString());
			}
			reader.endArray();
			
			/*read semanticTags*/
			if(!reader.nextName().equals("semanticTags")){
				reader.close();
				throw new IOException();
			}
			reader.beginArray();
			while(reader.hasNext()){
				post.getSemanticTags().add(reader.nextString());
			}
			reader.endArray();
			
			reader.endObject();
			posts.add(post);
		}
		reader.endArray();
		reader.close();
		
		return posts;
		
		
	}
	
	

}
