package dataManagement;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.csvreader.CsvWriter;


/**
 * This Class is responsible for the I/O with .csv files
 *
 */
public class CSVWriter {
	protected String mOutputDir;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd--HH-mm-ss");
	protected String csvName;
	private CsvWriter csvOutput;
	private String[] mColumnNames;
	/**
	 * @param columnNames  the names of the columns of the table you want to save
	 * @param outputDir the absolute directory of the output file
	 * @throws IOException
	 */
	public CSVWriter(String[] columnNames, String outputDir) throws IOException{
		mOutputDir = outputDir;
		mColumnNames = columnNames;
		setCsvName();
		csvOutput = new CsvWriter(mOutputDir + csvName);
		csvOutput.writeRecord(columnNames);		
	}
	
	/**
	 * @param data a record(line) of data
	 * @throws IOException
	 */
	public void store(String[] data) throws IOException {
		if (data.length != mColumnNames.length) {
			System.err.println("Wrong number of data");
			return;
		}
		csvOutput.writeRecord(data);
	}
	
	private void setCsvName(){
		Date currDate = new Date();		
		String dateString = sdf.format(currDate);
		csvName = dateString + ".csv";
	}
	
	public void close(){
		csvOutput.close();
	}
}
