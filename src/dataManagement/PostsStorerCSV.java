package dataManagement;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import com.csvreader.CsvWriter;

/**
 * This class is responsible of storing posts from the memory to disk in .csv format.
 *
 */
public class PostsStorerCSV extends CsvWriter  {
	protected static  String dir = "data\\";
	/**
	 * @param filename - full absolute path of file to store.
	 * @throws IOException
	 */
	public PostsStorerCSV(String filename) throws IOException{
		super(dir + filename,',',Charset.forName("UTF-8"));
		writeRecord(PostData.basicColumnNames);
	}
	
	/**
	 * @param posts list of {@link PostData} objects represnting the posts to save.
	 * @throws IOException
	 */
	public void store(List<PostData> posts) throws IOException{
		for (PostData post : posts){
			store(post);
		}		
	}
	
	/**
	 * @param post one post of type {@link PostData}
	 * @throws IOException
	 */
	public void store(PostData post) throws IOException{
		writeRecord(post.toBasicStringArray());
	}
	
}


