package dataManagement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import weka.core.Stopwords;
import dataProcessing.MessageProcessor;
import facebook4j.Comment;
import facebook4j.Like;
import facebook4j.Post;
import facebook4j.ResponseList;


/**
 * This class holds every post's data. <br/>
 * The data consists of: <br/>
 * - id of the post <br/>
 * - date <br/>
 * - the message of the post <br/>
 * - the type of the post (currently can be Picture, Video, Link or Regular) <br/>
 * - number of likes <br/>
 * - number of shares <br/>
 * - number of comments <br/>
 * - bag of words <br/>
 * - semantic tags <br/>
 * - popularity which can be Low, Medium or High
 *
 */
public class PostData {
	private String id;
	private Date date;
	private String message;
	private String type;
	private Integer likesNum;
	private Integer sharesNum;
	private Integer commentsNum;
	private Set<String> bagOfWords = new HashSet<String>(); /*instead of null*/
	private Set<String> semanticTags = new HashSet<String>(); /*instead of null*/
	private String popularity;
	
	
	public static String[] Popularities = {"Low","Medium","High"};
	private static int[]  PopularitySplits = {10481, 51949};
	static String[] basicColumnNames = {"Id","Date","Message","Type","LikesNum","SharesNum","CommentsNum"};
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd--HH-mm-ss");
	
	/**
	 * The constructor calculates the popularity and bag of words
	 * @param post the fetched post from facebook
	 * @param likes the list of likes
	 * @param comments the list of comments
	 */
	public PostData(Post post, ResponseList<Like> likes, ResponseList<Comment> comments){
		id = post.getId();
		date = post.getCreatedTime();
		message = post.getMessage();
		type = post.getType();
		if(likes !=null && likes.getSummary() != null && likes.getSummary().getTotalCount()!= null){
			likesNum =likes.getSummary().getTotalCount();
		} 
		if(comments!=null && comments.getSummary() != null && comments.getSummary().getTotalCount()!= null){
			commentsNum = comments.getSummary().getTotalCount();
		}
		if(post.getSharesCount() != null){
			sharesNum = post.getSharesCount();
		}
		calcBoW();
		calcPopularity();
	}

	/**
	 * 
	 * The constructor calculates the popularity and bag of words
	 * @param id id of the post
	 * @param date when the post is published
	 * @param message the text in the post
	 * @param type 
	 * @param likesNum
	 * @param sharesNum
	 * @param commentsNum
	 */
	public PostData(String id, Date date, String message, String type,
			Integer likesNum, Integer sharesNum, Integer commentsNum) {
		super();
		this.id = id;
		this.date = date;
		this.message = message;
		this.type = type;
		this.likesNum = likesNum;
		this.sharesNum = sharesNum;
		this.commentsNum = commentsNum;
		calcBoW();
		calcPopularity();
	}

	
	/**
	 * @param basicString - record consists basic post data - without bag of word, semantic tags and popularity.
	 */
	public PostData(String[] basicString){
		if(basicString.length != basicColumnNames.length){
			return;
		}
		this.id = basicString[0];
		try {
			this.date = sdf.parse(basicString[1]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.message = basicString[2];
		this.type = basicString[3];
		this.likesNum = basicString[4]==null?null:Integer.parseInt(basicString[4]);
		this.sharesNum = basicString[5]==null?null:Integer.parseInt(basicString[5]);
		this.commentsNum = basicString[6]==null?null:Integer.parseInt(basicString[6]);
		calcBoW();
		calcPopularity();
	}
	
	/**
	 * @return array of string consists post's basic data which is without bag-of-word, semantic tags and popularity.
	 */
	public String[] toBasicStringArray(){
		String[] data =  {
			id,
			sdf.format(date),
			message,
			type,
			likesNum==null?null:Integer.toString(likesNum),
			sharesNum==null?null:Integer.toString(sharesNum),
			commentsNum==null?null:Integer.toString(commentsNum)		
		};
		return data;
	}
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}
	
	public String getDateAsString() {
		if (date == null) {
			return null;
		}
		return sdf.format(date);
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getLikesNum() {
		return likesNum;
	}

	public void setLikesNum(Integer likesNum) {
		this.likesNum = likesNum;
	}

	public Integer getSharesNum() {
		return sharesNum;
	}

	public void setSharesNum(Integer sharesNum) {
		this.sharesNum = sharesNum;
	}

	public Integer getCommentsNum() {
		return commentsNum;
	}

	public void setCommentsNum(Integer commentsNum) {
		this.commentsNum = commentsNum;
	}

	public Set<String> getBagOfWords() {
		return bagOfWords;
	}


	public Set<String> getSemanticTags() {
		return semanticTags;
	}

	public void setSemanticTags(Set<String> semanticTags) {
		this.semanticTags = semanticTags;
	}

	private void calcBoW(){
		if(message !=null){
			for(String word : MessageProcessor.getUnigrams(message)){
				if(!Stopwords.isStopword(word)){
					bagOfWords.add(word);
				}
			}
//			bagOfWords.addAll(MessageProcessor.getNgrams(message,2));
		}
	}

	/**
	 * Calculates popularity of posts. <br/>
	 * Firstly calculates a number according to the formula: <i> 1*likesNum + 15* commentsNum + 15*sharesNum</i> </br>
	 * Then matches the popularity value according to fix pre-calculated splits of values ({@code PopularitySplits}). <br/>
	 * A popularity value may be: {@code Low} , {@code Medium} and {@code High}.
	 */
	private void calcPopularity(){
		if(likesNum==null || commentsNum == null || sharesNum == null){
			return;
		}
		int popularityVal =1*likesNum + 15* commentsNum + 15 * sharesNum;
		if(popularityVal <= PopularitySplits[0]){
			popularity = Popularities[0];
		} else if(popularityVal <= PopularitySplits[1]){
			popularity = Popularities[1];
		} else {
			popularity = Popularities[2];
		}
	}

	public String getPopularity() {
		return popularity;
	}
	
	public Integer getLikesLevel(){
		if (likesNum == null){
			return null;
		}
		return (likesNum < 3645.5)?0:(likesNum < 23034.5)?1:2;
	}
}
