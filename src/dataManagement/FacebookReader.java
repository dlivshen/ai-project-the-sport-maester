package dataManagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import facebook4j.Comment;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Like;
import facebook4j.Post;
import facebook4j.Reading;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;


/**
 * This class is responsible of reading and fetching posts from facebook. <br/>
 * To use this class you need an access_token from facebook.
 * To get one, please go to: <a href="https://developers.facebook.com/docs/facebook-login/access-tokens" >this link</a>
 * and paste the access token on the {@code ACCESS_TOKEN} variable 
 *
 */
public class FacebookReader {
	private Facebook facebook;
	private static String pageId = "338233632988842";
	public static String ACCESS_TOKEN = "CAACEdEose0cBALHLTgC9qdlbZAFbXCMZAZAXvq43teJ95JYP3ZBolgm5hWGLLmuA5hCNUg2BSFaTzrCS9QToEbUpRlubWanCnZByx7YXBhvo7lAu8XtiZB5ZBAFA2es0cleuQmcisor5JBnEy5YsBu0gXVmSsTq809wrZATJba9OEMYAtpATdvQcRM2jGv62TjKbY5Edg9bWVc7IumDSf60vKF370moDPJEZD";
	private static int MAX_PAGES = Integer.MAX_VALUE;
	
	public FacebookReader(){
		facebook = new FacebookFactory().getInstance();
		facebook.setOAuthAppId("", "");
		AccessToken at = new AccessToken(ACCESS_TOKEN);		
        facebook.setOAuthAccessToken(at);
        
	}
	/**
	 * @param since the date of posts to start fetching from
	 * @param until the date of posts to stop fetching in
	 * @return a list of {@link PostData} objects representing the fetched posts.
	 * @throws FacebookException
	 */
	public List<PostData> getPosts(Date since, Date until) throws FacebookException{
		List<PostData> allPosts = new ArrayList<PostData>();
		ResponseList<Post> posts = facebook.getPosts(pageId,new Reading().until(until).since(since));
		for(int i=0;i<MAX_PAGES;i++){
			for (Post post : posts) {
				ResponseList<Like> likes = facebook.getPostLikes(post.getId(),new Reading().summary());
	            ResponseList<Comment> comments = facebook.getPostComments(post.getId(),new Reading().summary());
	            
	            allPosts.add(new PostData(post,likes,comments));
	            System.out.print(".");
			}
            //get next page if exists
            if(posts.getPaging()==null){
	        	break;
	        }
	        posts = facebook.fetchNext(posts.getPaging());
	        if(posts == null){
	        	break;
	        }
			
		}
		return allPosts;
	}
}
