package dataManagement;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.stream.JsonWriter;




/**
 * This class is responsible of storing posts from memory to disk using .json format.
 *
 */
public class PostsStorerJSON {
	protected static  String dir = "data\\";
	private JsonWriter writer;
	public PostsStorerJSON(String filename) throws IOException {
		writer = new JsonWriter(new FileWriter(dir + filename));
		writer.setIndent("\t");
	}	
	/**
	 * @param posts list of {@link PostData} objects represnting the posts to save.
	 * @throws IOException
	 */
	public void write(PostData post) throws IOException {
		writer.beginObject();
		/*write basic data*/
		String[] names = PostData.basicColumnNames;
		String[] values = post.toBasicStringArray();		
		for(int i=0;i<names.length;i++){
			writer.name(names[i]).value(values[i]);
		}
		
		/*write bagOfWords:*/
		writer.name("bagOfWords").beginArray();
		for(String word : post.getBagOfWords()){
			writer.value(word);
		}
		writer.endArray();
		
		/*write semanticTags*/
		writer.name("semanticTags").beginArray();
		for(String tag : post.getSemanticTags()){
			writer.value(tag);
		}
		writer.endArray();
		
		writer.endObject();
	}
	
	public void write(List<PostData> posts) throws IOException{
		writer.beginArray();
		for(PostData post : posts){
			write(post);
		}
		writer.endArray();
	}
	
	public void close() throws IOException{
		writer.close();
	}

}
