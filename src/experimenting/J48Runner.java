package experimenting;


import weka.classifiers.trees.J48;
import weka.core.Instances;

public class J48Runner extends AbstractRunner {

	public J48Runner(Instances trainingData,Instances testData, String outputFilename, int minObjLeaf) throws Exception {
		super(trainingData, testData, outputFilename);
		J48 j48 = new J48();
		String[] options = {"-M",Integer.toString(minObjLeaf)};
		j48.setOptions(options);
		this.classifier=j48;		
	}
	
}
