package experimenting;

import java.util.List;

import weka.core.Instances;

import com.csvreader.CsvWriter;

import dataManagement.PostData;
import dataProcessing.ArffWriter;

public abstract class AbstractExperiment {
	protected Instances trainingData;
	protected Instances trainingDataOrig;
	protected List<PostData> testSetPosts;
	protected Instances testData;
	protected String className;
	protected String classifierName;
	protected String outputFilename;
	
	
	
	public AbstractExperiment(Instances trainingDataOrig,
			List<PostData> testSetPosts, String className) throws Exception {
		AttributesManipulator.removeRedundantFeatures(trainingDataOrig, className);
		this.trainingDataOrig = trainingDataOrig;
		this.trainingData = trainingDataOrig;
		this.testSetPosts = testSetPosts;
		this.className = className;
		outputFilename="./results/"+className.replace(" ", "");
	}

	private void prepareDataForRun() throws Exception{
		System.out.print("Selecting attributes...");
		trainingData = AttributesManipulator.selectAttributes(trainingDataOrig);
		System.out.println("done");
		System.out.print("Creating Test Set...");
		testData = ArffWriter.createTestSet(testSetPosts, trainingData);
		testData.setClassIndex(testData.numAttributes()-1);
		System.out.println("done");
	}
	
	abstract protected String[] getCsvColumnNames();	
	public String[] runSingleExperimentAux() throws Exception{
		prepareDataForRun();
		return runSingleExperiment();
	}
	abstract public String[] runSingleExperiment() throws Exception;
	
	public void runExperiment() throws Exception{
		System.out.println("\n-===-===-===-===-Big Experiment of "+className+" with "+classifierName+" is starting. Good Luck!");
		CsvWriter csvWriter = new CsvWriter(outputFilename+".csv");
		csvWriter.writeRecord(getCsvColumnNames());
		
		System.out.println("\n-=-=-=-Experimenting with all features-=-=-=-\n");
		csvWriter.write("Basic features + BoW + Tags");
		csvWriter.writeRecord(runSingleExperimentAux());
		
		System.out.println("\n-=-=-=-Experimenting without tag features-=-=-=-\n");
		//System.out.print("Removing attributes...");
		AttributesManipulator.removeTagFeatures(trainingDataOrig);
		//System.out.println("done");
		csvWriter.write("Basic features + BoW");
		outputFilename+="_nTAG";
		csvWriter.writeRecord(runSingleExperimentAux());
		
		System.out.println("\n-=-=-=-Experimenting without tag & bow features-=-=-=-\n");
		//System.out.print("Removing attributes...");
		AttributesManipulator.removeBowFeatures(trainingDataOrig);
		//System.out.println("done");
		csvWriter.write("Basic features");
		outputFilename+="nBOW";
		csvWriter.writeRecord(runSingleExperimentAux());
		
		csvWriter.close();
		System.out.println("-===-===-===-===-Congrats! Big Experiment is done.");
	}
}
