package experimenting;

import java.io.File;
import java.io.FileReader;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ArffSaver;

public class ExperimentsChecker {
	public static void main(String[] args) throws Exception {
		ArffReader arffReader = new ArffReader(new FileReader("./data/try3.arff"));
		Instances trainInstances = arffReader.getData();
//		arffReader = new ArffReader(new FileReader("./test_stable_f2_likesnom_select.arff"));
//		Instances testInstances = arffReader.getData();
		trainInstances.setClassIndex(trainInstances.numAttributes()-1);
//		testInstances.setClassIndex(trainInstances.numAttributes()-1);
//		System.out.println(Experimenter.experimentJ48(trainInstances, testInstances, "./res"));
		AttributesManipulator.removeRedundantFeatures(trainInstances, "Likes Level");
//		Instances selected = AttributesManipulator.selectAttributes(trainInstances); 
		AttributesManipulator.removeTagFeatures(trainInstances);
		
		ArffSaver saver = new ArffSaver();
		saver.setInstances(trainInstances);
		saver.setFile(new File("./data/try3_select.arff"));
//		saver.setDestination(new File(destFileName));
		saver.writeBatch();
	}
}
