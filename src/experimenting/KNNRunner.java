package experimenting;

import java.io.IOException;

import weka.classifiers.lazy.IBk;
import weka.core.Instances;

public class KNNRunner extends AbstractRunner {

	public KNNRunner(Instances trainingData,Instances testData, String outputFilename, int k) throws IOException {
		super( trainingData, testData, outputFilename);
		IBk knn = new IBk(k);
		this.classifier = knn;
	}
	
}
