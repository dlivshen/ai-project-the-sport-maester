package experimenting;

import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.BestFirst;
import weka.attributeSelection.CfsSubsetEval;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;
import dataProcessing.ArffWriter;

public class AttributesManipulator {
	static String[] BaseFeatures = {"date","Message","Type","LikesNum","SharesNum","CommentsNum","Popularity","Likes Level"};
	/*after this, the class Att is last.*/
	public static void removeRedundantFeatures(Instances trainingData,String classAttName) throws Exception{
		/*assert the class exists*/
		if(!(classAttName.equals("LikesNum")||classAttName.equals("SharesNum")||classAttName.equals("CommentsNum")||
				classAttName.equals("Popularity")||classAttName.equals("Likes Level"))){
			throw new Exception("Class name doesn't exist!");
		}
		
		for(int i=BaseFeatures.length-1;i>=0;i--){
			if(!classAttName.equals(BaseFeatures[i]) && i!=2 ){
				trainingData.deleteAttributeAt(i);
			} 
		}
		
		trainingData.setClassIndex(1);
	}
	public static void removeBowFeatures(Instances trainingData){
		for(int i=trainingData.numAttributes()-1;i>=0;i--){
			if(trainingData.attribute(i).name().startsWith(ArffWriter.sWordPrefix)){
				try {
					trainingData.deleteAttributeAt(i);
					} catch (Exception e) {
						System.out.println("Failed to delete BoW feature, skipping");
					}
			}
		}	
	}
	public static void removeTagFeatures(Instances trainingData){
		for(int i=trainingData.numAttributes()-1;i>=0;i--){
			if(trainingData.attribute(i).name().startsWith(ArffWriter.sTagPrefix)){
				try {
				trainingData.deleteAttributeAt(i);
				} catch (Exception e) {
					System.out.println("Failed to delete tag feature, skipping");
				}
			}
		}	
	}
	public static Instances selectAttributes(Instances trainingData) throws Exception{
		AttributeSelection as = new AttributeSelection();
		ASEvaluation ase = new CfsSubsetEval();
		ase.buildEvaluator(trainingData);
		ASSearch ass = new BestFirst();
		as.setEvaluator(ase);
		as.setSearch(ass);
		as.setInputFormat(trainingData);
		return Filter.useFilter(trainingData, as);
	}
}
