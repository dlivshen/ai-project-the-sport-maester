package experimenting;

import java.util.List;

import weka.core.Instances;
import dataManagement.PostData;

public class KNNExperimenter extends AbstractExperiment {
	
	static private int minK = 1;
	static private int maxK = 11;
	
	public KNNExperimenter(Instances trainingDataOrig,
			List<PostData> testSetPosts, String className) throws Exception {
		super(trainingDataOrig, testSetPosts, className);
		classifierName="KNN";
		outputFilename+="_KNN";
	}

	@Override
	protected String[] getCsvColumnNames() {
		String[] result = new String[(maxK-minK)/2+2];
		result[0] = "K";
		for(int k = minK;k<=maxK;k+=2){
			result[(k-minK)/2+1]=Integer.toString(k);
		}
		return result;		
	}

	@Override
	public String[] runSingleExperiment() throws Exception {
		String[] result = new String[(maxK-minK)/2+1];
		for(int k = minK;k<=maxK;k+=2){
			System.out.println("Running KNN with k = "+ Integer.toString(k) + " :");
			String runOutputFilename = outputFilename + "_K" + Integer.toString(k) +".txt";
			double tmpRes = new KNNRunner(trainingData, testData, runOutputFilename, k).runClassifier();
			result[(k-minK)/2]=String.format( "%.2f", tmpRes);
			System.out.println();
		}
		return result;
	}

}
