package experimenting;

import java.util.List;

import weka.core.Instances;
import dataManagement.PostData;

public class J48Experiment extends AbstractExperiment {
	
	private static int minObjLeafMin = 2 ;
	private static int minObjLeafMax = 8 ;
	
	public J48Experiment(Instances trainingDataOrig,List<PostData> testSetPosts, String className) throws Exception {
		super(trainingDataOrig, testSetPosts, className);
		classifierName="J48";
		outputFilename+="_J48";
	}

	@Override
	protected String[] getCsvColumnNames() {
		String[] result = new String[minObjLeafMax-minObjLeafMin+2];
		result[0] = "minObjLeaf";
		for(int minObjLeaf=minObjLeafMin;minObjLeaf<=minObjLeafMax;minObjLeaf++){
			result[minObjLeaf-minObjLeafMin+1]=Integer.toString(minObjLeaf);
		}
		return result;
	}

	@Override
	public String[] runSingleExperiment() throws Exception {
		String[] result = new String[minObjLeafMax-minObjLeafMin+1];
		for(int minObjLeaf=minObjLeafMin;minObjLeaf<=minObjLeafMax;minObjLeaf++){
			System.out.println("Running J48 with minObjLeaf = "+ Integer.toString(minObjLeaf) + " :");
			String runOutputFilename = outputFilename + "_M" + Integer.toString(minObjLeaf) +".txt";
			double tmpRes = new J48Runner(trainingData, testData, runOutputFilename, minObjLeaf).runClassifier();
			result[minObjLeaf-minObjLeafMin]=String.format( "%.2f", tmpRes);
			System.out.println();
		}
		return result;
	}

}
