package experimenting;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import dataManagement.PostData;
import dataManagement.PostsLoaderCSV;
import dataProcessing.DataOrganizer;



public class Experimenter {

	private static String trainingDataFilename = "./data/stable_f2_sparse.arff"; //should be sparse
	private static String testDataCsv = "test.csv";//without ./data/ prefix

	public static void main(String[] args) throws Exception {
		
		List<PostData> testSetData = PostsLoaderCSV.loadPosts(testDataCsv);
		System.out.print("Creating semantic attributes for test set...");
		DataOrganizer.processPosts(testSetData);
		
		/*The Experiments*/
		/*Nominal J48 Experiments*/
		new J48Experiment(getTrainingData(),testSetData,"Likes Level").runExperiment();
//		new J48Experiment(getTrainingData(),testSetData,"Popularity").runExperiment();
		
		/*Nominal KNN Experiments*/
		new KNNExperimenter(getTrainingData(), testSetData, "Likes Level").runExperiment();
//		new KNNExperimenter(getTrainingData(), testSetData, "Popularity").runExperiment();
		
		/*Numeric KNN Experiments*/
		new KNNExperimenter(getTrainingData(), testSetData, "LikesNum").runExperiment();
		new KNNExperimenter(getTrainingData(), testSetData, "SharesNum").runExperiment();
		new KNNExperimenter(getTrainingData(), testSetData, "CommentsNum").runExperiment();
	}
	
	private static Instances getTrainingData() throws FileNotFoundException, IOException{
		ArffReader arffReader = new ArffReader(new FileReader(trainingDataFilename));
		return arffReader.getData();
	}
	


	

}
