package experimenting;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.Instances;


public abstract class AbstractRunner {
	private FileWriter output;
	protected Classifier classifier;
	protected Instances trainingData;
	protected Instances testData;
	protected Attribute classAtt;	
	
	
	public AbstractRunner(Instances trainingData, Instances testData,String outputFilename) throws IOException {		
		
		this.trainingData = trainingData;
		this.testData = testData;
		this.classAtt = trainingData.classAttribute();
		this.output = new FileWriter(outputFilename);
	}
	
	static String[] RemovableFeatures = {"date","Message","LikesNum","SharesNum","CommentsNum","Popularity","Likes Level"};


	double runClassifier() throws Exception{
		
		double result;
		System.out.print("Building classifier...");
		classifier.buildClassifier(trainingData);
		output.write(classifier.toString());
		System.out.println("done");
		
		System.out.print("Evaluating 10-fold cross validation...");
		Evaluation eval = new Evaluation(trainingData);
		eval.crossValidateModel(classifier, trainingData, 10, new Random(1));
		writeInterestingStats(eval,"\n-=-=-=-=-=-Cross Validation Results-=-=-=-=-=-\n");
		result = getInterestingInfo(eval);
		System.out.println("done");
		
		if(testData!=null){
			System.out.print("Evaluating Test Set...");
			eval.evaluateModel(classifier,testData);
			writeInterestingStats(eval,"\n-=-=-=-=-=-Test Set Results-=-=-=-=-=-\n");
			System.out.println("done");
		}
		
		output.close();
		return result;
		
	}
	/*returns the interesting data from the classifier run, according to the class type*/
	private double getInterestingInfo(Evaluation eval) throws Exception{
		Attribute classAtt = trainingData.classAttribute();
		if(classAtt.type() == Attribute.NOMINAL){
			return eval.pctCorrect();
		}else if(classAtt.type() == Attribute.NUMERIC){
			return eval.correlationCoefficient(); //check
		} else {
			throw new Exception("Class attribiute must be numeric or nominal");
		}
	}
	private void writeInterestingStats(Evaluation eval,String title) throws Exception{
		
		if(classAtt.type() == Attribute.NOMINAL){
			output.write(eval.toMatrixString(title));
			output.write(eval.toClassDetailsString());
			output.write(eval.toSummaryString());
		}else if(classAtt.type() == Attribute.NUMERIC){
			output.write(eval.toSummaryString(title,false));
		} else {
			throw new Exception("Class attribiute must be numeric or nominal");
		}
	}
	
}
